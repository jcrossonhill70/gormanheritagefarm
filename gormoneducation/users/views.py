# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import print_function

from django.shortcuts import render

def organizerAccountCreate(request):
    '''
    account creation portal for field trip organizers
    '''
    siteDict = {}

    return render(request, 'users/organizerPortal.html', siteDict)

def volunteerAccountCreate(request):
    '''
    account creation portal for volunteer educator
    '''
    siteDict = {}

    return render(request, 'users/volunteerPortal.html', siteDict)

def programSchedule(request):
    '''
    portal for viewing booking, confirmation, payment, cancellations
    '''
    siteDict = {}

    return render(request, 'users/programSchedule.html', siteDict)
