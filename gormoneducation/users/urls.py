from django.conf.urls import url
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
	url(r'^organizer', views.organizerAccountCreate, name = 'organizerAccountCreate'),
    url(r'^educator', views.volunteerAccountCreate, name = 'volunteerAccountCreate'),
    url(r'^programs', views.programSchedule, name = "programSchedule"),
]
